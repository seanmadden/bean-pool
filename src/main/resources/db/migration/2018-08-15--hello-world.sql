-- auto-generated definition
create table transactions
(
  transaction_id serial     not null
    constraint transactions_pkey
    primary key,
  sender         varchar    not null,
  amount         numeric(2) not null,
  message        varchar
);

alter table transactions
  owner to postgres;

