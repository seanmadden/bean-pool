package com.sdmadden.beanpool

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BeanPoolApplication

fun main(args: Array<String>) {
    runApplication<BeanPoolApplication>(*args)
}
