package com.sdmadden.beanpool

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping

@Controller
class ContributionController(
        val contributionService: ContributionService
) {

    @GetMapping("/contrib")
    fun contrib() {
        contributionService.doSomething()
    }

    @GetMapping("/contributions")
    fun contribuions(model: Model): String {
        val contribs = contributionService.getContributions()

        model.addAttribute("contributions", contribs)
        return "contributions"
    }
}