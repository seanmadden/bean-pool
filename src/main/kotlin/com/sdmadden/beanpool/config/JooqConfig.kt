package com.sdmadden.beanpool

import org.jooq.DSLContext
import org.jooq.impl.DSL
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class JooqConfig {
    @Value("\${spring.datasource.url}")
    lateinit var connectionString: String

    @Value("\${spring.datasource.username}")
    lateinit var user: String

    @Value("\${spring.datasource.password}")
    lateinit var password: String

    @Bean
    fun jooq() : DSLContext {
        return DSL.using(
                connectionString,
                user,
                password
        )
    }
}