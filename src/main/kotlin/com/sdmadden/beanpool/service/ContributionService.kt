package com.sdmadden.beanpool

import com.sdmadden.beanpool.tables.Transactions
import org.springframework.stereotype.Service
import org.jooq.*
import org.jooq.impl.DSL
import org.jooq.impl.DSL.*
import java.math.BigDecimal
import java.util.*


@Service
class ContributionService(
        val jooq: DSLContext
) {
    fun doSomething() {

        jooq.use { ctx ->
            ctx.insertInto(table("transactions"),
                    field("sender"),
                    field("amount"))
                    .values("Sean",
                            BigDecimal.valueOf(5))
                    .execute()
        }
    }

    fun getContributions(): List<com.sdmadden.beanpool.tables.pojos.Transactions> {
        val transactions = Transactions()
        return jooq.selectFrom(transactions)
                .fetch()
                .into(com.sdmadden.beanpool.tables.pojos.Transactions::class.java)
                .toList()
    }
}